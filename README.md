
# sorensen

European Spallation Source ERIC, Site-specific EPICS module : sorensen

This module provides device support for the (Ametek) Sorensen SG Series family of programmable DC power supplies. The device support is based on the LAN interface, and can therefore only be used for power supplies that has the optional LAN module.
- Manufacturer page: https://www.powerandtest.com/power/dc-power-supplies/sg-series
- Technical manual: docs/SGA-Series_Operation_Manual_M550129-01_rAD.pdf
- LAN Interface User Manual: docs/SG_IEEE-488_RS232_Ethernet_Prog_Manual_M550129-03_rvJ.pdf

(Check the manufacturer site for possible updates.ß)

## Dependencies

StreamDevice

### Using the module


In order to use the device support module declare a require statement for streamdevice and sorensen:

    require streamdevice [,version]
    require sorensen [, version]

Then load the module:

    iocshLoad("$(sorensen_DIR)/sorensen.iocsh", "Ch_name=<CONNECTIONNAME>, IPADDR=<IP address>, P=<system-subsystem>:, R=<discipline-device-index>:, Amp2TeslaCalib=<conv_factor>")

__NOTE:__ Remember to supply the colons after P and R!

### Parameters:

- CONNECTIONNAME: The name of the asyn IP port connection. For trouble shooting purposes.
- IPADDR: The IP address of your power supply LAN module
- P: The section and subsection part of your device name (include the colon)
- R: The discipline, device and index part of your device name (include the colon)
- Amp2TeslaCalib: factor for conversion from current to magnetic field

The following macros have defaults so they are not obligatory to instantiate:

- MINVOL: The maximum voltage of your power supply (e.g. 10V)
- MAXVOL: The maximum voltage of your power supply (e.g. 10V)
- MINCUR: The maximum current of your power supply (e.g. 500A)
- MAXCUR: The maximum current of your power supply (e.g. 500A)


This module has no code to compile, otherwise it is a regular EPICS module.

* Run `make` from the command line.
